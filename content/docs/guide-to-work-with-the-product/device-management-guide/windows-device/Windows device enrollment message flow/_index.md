
# Windows Device Enrollment Process Message Flow

## Overview

Windows devices are enrolled through the inbuilt application of the respective device, i.e Company app on Windows 8.0 , Workplace on Windows 8.1 and Work Access on Windows 10\. The endpoints it triggers cannot be altered as required by the service developer. Therefore additional server configurations are required to manage Windows devices using Entgra IoTS.





For more information, see [Windows Server Configurations](/doc/en/lb2/Windows-Server-Configurations.html).





The high level message flow of a Windows device is shown in the below diagram. The Windows device sends requests to a Proxy server that then directs the requests to the Entgra IoT Server. 

![image](352824771.png)

## How it works

#### Step 1: Initiate the enrollment process

*   When a user signs in through the Workplace application using a Windows device, the user's email address needs to be provided as shown below:





    The email address is provided in the following format: `<user>@<EMAIL_DOMAIN>`.





*   The automatic discovery service of the device uses the following fields and constructs a unique URI:
    *   Append the subdomain `enterpriseenrollment`.
    *   Extracts and append the domain from the username, i.e `<EMAIL_DOMAIN>`, that was submitted when signing in.
    *   Append the path `/ENROLLMENTSERVER/Discovery.svc`.





    The constructed URI will take the following format: `enterpriseenrollment.<EMAIL_DOMAIN>/ENROLLMENTSERVER/Discovery.svc`

    Example: If the given user name is `admin@wso2.com` the resulting URI would be `enterpriseenrollment.wso2.com/ENROLLMENTSERVER/Discovery.svc`.





*   The constructed URI will then be used to initiate the enrollment process of the mobile device.

### Step 2: Proxy endpoint 01

**URI format**


http://enterpriseenrollment.<EMAIL_DOMAIN>/ENROLLMENTSERVER/discovery.svc


*   The first Proxy endpoint known as the `Discovery Service Endpoint` receives the initial request from the device. 





    The initial request from the device is a standard `HTTP GET` request.





*   The received request is redirected by the Proxy Server to MDM endpoint 01 at the server end.





    The `GET` request helps the device to check if the server is up and running.





*   Once a success message is returned to the device by MDM endpoint 01, It will move to the next step.

#### Step 3: MDM endpoint 01

**URI format**

`http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/discovery/get `

*   This endpoint receives the initial `GET` request that was redirected by the Proxy Server. It then returns a success response to the device. 
*   When the endpoint receives the success message from the server, the device triggers a `HTTPS POST` request to the same proxy endpoint (Proxy endpoint 01) .
    The Proxy Server redirects the request to MDM endpoint 02, which is at the server side.

#### Step 4: MDM endpoint 02

**URI format**

`http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/discovery/post `

*   Once the `POST` request from the device is received at this endpoint, the MDM server will include the following details within a response body and send to the device. 
*   If Requested user device is Windows 8/8.1 one, user gets 3.a proxy endpoints for the response of the POST request, But If its a Windows 10 Device, He/She gets 3.b proxy endpoint.
    1\. Authentication Policy.(Federated)
    2\. Proxy endpoint for the Enrollment policy. (`/ENROLLMENTSERVER/PolicyEnrollmentWebservice.svc`) - Proxy endpoint 02.
    3\. a. Proxy endpoint for the Enrollment Service. (`/ENROLLMENTSERVER/DeviceEnrollmentWebservice.svc`) - Proxy endpoint 03 a

        b. Proxy endpoint for the Enrollment Service. (`/ENROLLMENTSERVER/Win10DeviceEnrollmentWebservice.svc`) - Proxy endpoint 03 b

    4\. Proxy endpoint for the Windows login page. (`/windows-web-agent`) - Proxy endpoint for the federated login page.

     First of all device receive proxy end point for federated login page as following:

`https://enterpriseenrollment.<EMAIL_DOMAIN>/emm/enrollments/windows/login-agent`

*   After receiving the above responses, the device requests are made in the following order:
    1.  The device requests for the login page using the proxy endpoint URL. The proxy server will then route the request to the following IoTS endpoint.

        `http://<server-ip>:<server-port>/windows-web-agent`

    2.  Next the device sends a request to Proxy endpoint 2 and 3 respectively.

#### Step 5: Proxy endpoint 02

**URI format**

`https://enterpriseenrollment.<EMAIL_DOMAIN>/ENROLLMENTSERVER/policyenrollmentwebservice.svc `

This Proxy Server routes the messages to MDM endpoint 03.

#### Step 6: MDM endpoint 03

**URI format**

`http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/certificatepolicy/xcep`

*   MDM Endpoint 3 responds back to the device with the Certificate Enrollment Policy.





    The Certificate Enrollment Policy is an implementation of the MS-XCEP 509 protocol.





    Once the certificate enrollment policy is received, the device sends the Certificate Signing Request (CSR) to **Proxy endpoint 03**.

#### **Step 7: Proxy endpoint 03\. a**

**URI format**

`https://enterpriseenrollment.<EMAIL_DOMAIN>/ENROLLMENTSERVER/deviceenrollmentwebservice.svc `

This Proxy Server routes the message received from MDM endpoint 03 a. to **MDM endpoint 04.a**

#### **Step 8: Proxy endpoint 03\. b**

**URI format**

`https://enterpriseenrollment.<EMAIL_DOMAIN>/ENROLLMENTSERVER/Win10deviceenrollmentwebservice.svc `

This Proxy Server routes the message received from MDM endpoint 03 b. to **MDM endpoint 04.b**

#### Step 8: MDM Endpoint 04\. a

**URI format**

`http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/deviceenrolment/wstep`

*   The service corresponding to this endpoint generates the signed certificate for the Certificate Signing Request and provide next enrollment Proxy endpoint **Proxy Endpoint 4.a**
*   It then responds to the device with an encoded `wap-provisioning.xml` file, which includes necessary certificates and other Device Management information.





The certificate enrollment is an implementation of the `MS-WSTEP` protocol.

The initial details provided through the `POST` request are used to persist the device details in the database.





#### Step 8: MDM Endpoint 04\. b

**URI format**

`http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/deviceenrolment/enrollment`

*   The service corresponding to this endpoint generates the signed Certificate for the Certificate Signing Request and Enroll the Windows 10 devices against the IOT server and provide the get pending Proxy endpoint **Proxy Endpoint 4.b**

#### Step 9: Proxy Endpoint 04\. a

**URI format**

`https://enterpriseenrollment.<EMAIL_DOMAIN>/syncml/initialquery`

This Proxy Server routes the message received to **MDM endpoint 05**.

At the end of this flow Device is enrolled successfully.

#### **Step 9: Proxy Endpoint 04\. b**

**URI format**

`https://enterpriseenrollment.<EMAIL_DOMAIN>/devicemgt`

This Proxy Server routes the message received to **MDM endpoint 06**.

#### **Step 10: MDM Endpoint 05**

**URI format**

`http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/services/syncml/devicemanagement/request`

#### **Step 10: MDM Endpoint 06**

**URI format**

`http://<server-ip>:<server-port>/api/device-mgt/windows/v1.0/management/devicemgt/pending-operations`

This endpoint will handle all the device management requests and responses. 





The protocol that is being used is SyncML v1.2.
